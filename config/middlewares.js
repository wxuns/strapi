module.exports = [
  'strapi::errors',
  {
    name: "strapi::security",
    config: {
      contentSecurityPolicy: {
        directives: {
          "script-src": ["'self'", "editor.unlayer.com", "*.tinymce.com", "*.tiny.cloud", "https:", "cdn.115113.xyz", "changyan.sohu.com"],
          "script-src-elem": ["'self'", "editor.unlayer.com", "*.tinymce.com", "*.tiny.cloud", "blob:", "*.strapi.io", "cdn.115113.xyz", "unsafe-inline", "changyan.sohu.com"],
          "frame-src": ["'self'", "editor.unlayer.com", "changyan.sohu.com"],
          "img-src": [
            "'self'",
            "data:",
            "blob:",
            "cdn.jsdelivr.net",
            "*.strapi.io",
            "images.lizhixu.cn",
            "s3.amazonaws.com",
            "*.tinymce.com",
            "*.tiny.cloud",
            "dl.airtable.com"
          ],
          "style-src": [
            "'self'",
            "'unsafe-inline'",
            "*.tinymce.com",
            "*.tiny.cloud",
          ],
          "font-src": ["'self'", "*.tinymce.com", "*.tiny.cloud"],
          "media-src": [
            "'self'",
            "data:",
            "blob:"
          ],
          upgradeInsecureRequests: null,
        },
      },
    },
  },
  'strapi::cors',
  'strapi::poweredBy',
  'strapi::logger',
  'strapi::query',
  'strapi::body',
  'strapi::session',
  'strapi::favicon',
  'strapi::public',
];

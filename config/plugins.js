module.exports = ({ env }) => ({
  "users-permissions": {
    config: {
      register: {
        allowedFields: ["avatar", "sitemap_exclude"],
      },
    },
  },
  'entity-notes': {
		enabled: true,
	},
  "orama-cloud": {
    config: {
      privateApiKey: env('ORAMA_PRIVATE_API_KEY'),
    },
  },
  'sitemap': {
    enabled: true,
    config: {
      cron: '0 0 0 * * *',
      limit: 45000,
      xsl: true,
      autoGenerate: false,
      caching: true,
      allowedFields: ['id', 'uid'],
      excludedTypes: [],
    },
  },
  email: {
    config: {
      provider: 'strapi-provider-email-resend',
      providerOptions: {
        apiKey: env('RESEND_API_KEY'), // Required
      },
      settings: {
        defaultFrom: 'admin@lizhixu.cn',
        defaultReplyTo: 'admin@lizhixu.cn',
      },
    }
  }
});

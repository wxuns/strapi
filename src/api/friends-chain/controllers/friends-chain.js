'use strict';

/**
 * friends-chain controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::friends-chain.friends-chain');

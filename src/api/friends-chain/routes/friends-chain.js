'use strict';

/**
 * friends-chain router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::friends-chain.friends-chain');

'use strict';

/**
 * friends-chain service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::friends-chain.friends-chain');

'use strict';

/**
 * translate-config controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::translate-config.translate-config');

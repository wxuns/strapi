'use strict';

/**
 * translate-config service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::translate-config.translate-config');

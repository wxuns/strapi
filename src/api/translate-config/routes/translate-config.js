'use strict';

/**
 * translate-config router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::translate-config.translate-config');

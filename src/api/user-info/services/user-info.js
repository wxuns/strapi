'use strict';

module.exports = {
  async processUserInfo(user) {
    // 实现复杂逻辑
    // 例如，获取用户的相关数据或进行某些计算
    return {
      is_login: 1,
      user: {
        user_id: user.id,
        nickname: user.username,
        img_url: user?.avatar?.url
          ? (process.env.CDN_DOMAIN + user?.avatar?.url)
          : (process.env.DICEBEAR_DOMAIN + user.username),
        profile_url: '',
        sign: 1
      }
    };
  },
  async noUserInfo(callback) {
    const data = {
      is_login: 0,
      user: {}
    }
    return `${callback}(${JSON.stringify(data)})`
  }
};

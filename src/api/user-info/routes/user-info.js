module.exports = {
  routes: [
    {
      method: 'GET',
      path: '/user-info',
      handler: 'user-info.getUserInfo',
      config: {
        auth: false, // 如果需要身份验证，将其设置为true
      },
    },
    {
      method: 'GET',
      path: '/user-logout',
      handler: 'user-info.getUserLogout',
      config: {
        auth: false, // 如果需要身份验证，将其设置为true
      },
    },
  ],
};
